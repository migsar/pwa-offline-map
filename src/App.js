import React, { useState, useEffect } from 'react';
import ReactMapGL from 'react-map-gl';

import defaultMapStyle from './default-map-style.json';
import './App.css';

function App() {
  const [viewport, setViewport] = useState({
    width: '100vw',
    height: '100vh',
    latitude: 41.1381,
    longitude: -8.6108,
    zoom: 10
  });

  const [userLocation, setUserLocation] = useState({
    lat: 41.147870632102823,
    lng: -8.611099803264114
  });

  useEffect(() => {
    function success(pos) {
      const { latitude, longitude } = pos.coords;
      const lat = latitude.toFixed(6);
      const lng = longitude.toFixed(6);

      if ((lat !== userLocation.lat) || (lng !== userLocation.lng)) {
        console.log(`Update user location to: ${longitude}, ${latitude}`);
        setUserLocation({ lat, lng });
      }
    }
    
    function error(err) {
      console.warn('ERROR(' + err.code + '): ' + err.message);
    }
    
    if (navigator) {
      navigator.geolocation.watchPosition(success, error, {
        enableHighAccuracy: false,
        timeout: 5000,
        maximumAge: 0
      });
    }

    return function cleanup() {

    };
  }, []);

  defaultMapStyle.sources = {
    ...defaultMapStyle.sources,
    location: {
      type: 'geojson',
      data: { "type": "Feature", "properties": { "Name": "Aliados" }, "geometry": { "type": "Point", "coordinates": [userLocation.lng, userLocation.lat] } }
    },
  };

  return (
    <div className="App">
      <div>
        <h1>Location: {userLocation.lng}, {userLocation.lat}</h1>
      </div>
      <ReactMapGL
        {...viewport}
        mapStyle={{...defaultMapStyle}}
        onViewportChange={setViewport}
      />
    </div>
  );
}

export default App;